## Changelog

Keeping a changelog is important, but can lead to lots of merge conflicts.
GitLab dealt with by staging the changelog entry in an `changelogs/unreleased`
directory. For the full rationale, read the [blog post][blog]. To aid the new
workflow a script was created, named `changelog`. This script has been copied to
other projects too, but in a different location. This project is intended as the
one binary in your $PATH, to rule all the scripts.

### How to install

Provided you have your $GOPATH setup:

```bash
GO111MODULE=on go install gitlab.com/zj/changelog

# In the repository
changelog -m 1 --type added A new changelog entry will be recorded
```

[blog]: https://about.gitlab.com/2018/07/03/solving-gitlabs-changelog-conflict-crisis/
