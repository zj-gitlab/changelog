package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"
)

func newApp() *cli.App {
	app := cli.NewApp()
	app.Name = "changelog"
	app.Copyright = "Zeger-Jan van de Weg"
	app.Version = version
	app.Usage = "Generate changelog entries for a repository"

	app.Commands = []cli.Command{
		{
			Name:   "init",
			Usage:  "Create skeleton files to use this tool for this repository",
			Action: initAction,
		},
	}

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:  "merge-request, m",
			Usage: "Merge Request ID",
		},
		cli.StringFlag{
			Name:  "type, t",
			Usage: fmt.Sprintf("The category of the change, valid options are: %s", strings.Join(typeCategories(), ", ")),
		},
		cli.BoolFlag{
			Name:  "amend",
			Usage: "Amend the previous commit",
		},
		cli.BoolFlag{
			Name:  "dry-run, n",
			Usage: "Don't actually write anything, just print",
		},
		cli.BoolFlag{
			Name:  "force, f",
			Usage: "Overwrite an existing entry",
		},
		cli.BoolFlag{
			Name:  "git-username, u",
			Usage: "Use Git user.name configuration as the author",
		},
	}

	app.Action = createEntryAction

	return app
}

func initAction(c *cli.Context) error {
	rootDir, err := repoRoot()
	if err != nil {
		return fmt.Errorf("Not a valid Git repository: %s", err)
	}

	unreleasedDir := filepath.Join(rootDir, "changelogs", "unreleased")
	if err := os.MkdirAll(unreleasedDir, 0755); err != nil {
		return err
	}

	keepFile := filepath.Join(unreleasedDir, ".gitkeep")
	if err := ioutil.WriteFile(keepFile, []byte{}, 0644); err != nil {
		return err
	}

	clogFile := filepath.Join(rootDir, "CHANGELOG.md")
	if _, err := os.Stat(clogFile); os.IsNotExist(err) {
		return ioutil.WriteFile(clogFile, []byte("# Changelog\n\n"), 0644)
	}

	return nil
}

func createEntryAction(c *cli.Context) error {
	if !onFeatureBranch() {
		return errors.New("changelog entries can only be created on feature branches")
	}

	e, err := newEntry(c.Int("merge-request"), c.String("type"), entryTitle(c), entryAuthor(c))
	if err != nil {
		return err
	}

	p, err := filePath()
	if err != nil {
		return err
	}

	if !c.Bool("dry-run") {
		if err := e.writeToPath(p, c.Bool("force")); err != nil {
			return err
		}

		if c.Bool("amend") {
			if err := stageForCommit(p); err != nil {
				return err
			}

			if err := amendCommit(); err != nil {
				return err
			}
		}
	}

	out, err := e.marshal()
	fmt.Printf("%s\n", out)
	return err
}

func entryTitle(c *cli.Context) string {
	if c.Bool("amend") {
		commitMsg, err := lastCommitMessage()
		if err != nil {
			fmt.Println(err)
			return ""
		}

		return commitMsg
	}

	return strings.Join(c.Args(), " ")
}

func entryAuthor(c *cli.Context) string {
	if c.Bool("git-username") {

		a, err := getConfigKey("user.name")
		if err != nil {
			fmt.Println(err)
		}
		return a

	}

	return ""
}
