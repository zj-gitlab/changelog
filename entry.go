package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

var ErrNoTitle = errors.New("no title provided")

type entry struct {
	Id     int    `yaml:"merge_request,omitempty"`
	Title  string `yaml:"title"`
	Type   string `yaml:"type,omitempty"`
	Author string `yaml:"author,omitempty"`
}

// newEntry will
func newEntry(mrID int, tipe, title, author string) (*entry, error) {
	e := &entry{Id: mrID, Type: tipe, Title: title, Author: author}
	if e.Title == "" {
		return nil, ErrNoTitle
	}

	if e.Type == "" {
		e.Type = promptType()
	}

	return e, nil
}

// WriteToPath will generate the YAML and write it to the path on disk as
// passed as path
func (e *entry) writeToPath(path string, force bool) error {
	_, err := os.Lstat(path)
	switch {
	case os.IsNotExist(err):
		break
	case force && err == nil:
		break
	default:
		return fmt.Errorf("%s already exists! Use `--force` to overwrite.", path)
	}

	if err != nil && !os.IsNotExist(err) {
		return err
	}

	out, err := e.marshal()
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, out, 0644)
}

func (e *entry) marshal() ([]byte, error) {
	return yaml.Marshal(e)
}

func filePath() (string, error) {
	br, err := branchName()
	if err != nil {
		return "", err
	}

	cleanBr := regexp.MustCompile("/[^\\w]/").ReplaceAllString(br, "-")

	repoDir, err := repoRoot()
	if err != nil {
		return "", err
	}

	return filepath.Join(repoDir, "changelogs", "unreleased", fmt.Sprintf("%s.yml", cleanBr)), nil
}

var (
	types = [][2]string{
		[2]string{"added", "New feature"},
		[2]string{"fixed", "Bug fix"},
		[2]string{"changed", "Feature change"},
		[2]string{"deprecated", "New deprecation"},
		[2]string{"removed", "Feature removal"},
		[2]string{"security", "Security fix"},
		[2]string{"performance", "Performance improvement"},
		[2]string{fallBackType, "Other"},
	}

	fallBackType = "other"
)

func typeForIndex(i int) string {
	if i < 1 || i >= len(types)+1 {
		return fallBackType
	}

	return types[i-1][0]
}

func typeCategories() []string {
	cats := make([]string, len(types))

	for i, _ := range types {
		cats[i] = types[i][0]
	}

	return cats
}

// detectType will read the flag value, and if unable to get a type of the entry
// it will prompt the user
// If an error occurs, these will be written to STDOUT, and the fall back type
// will be used
func promptType() string {
	fmt.Println(">> Please specify the index for the category of your change:")
	for i, t := range types {
		fmt.Printf("%d. %s\n", i+1, t[1])
	}
	fmt.Print("?> ")

	reader := bufio.NewReader(os.Stdin)
	in, err := reader.ReadString('\n')
	if err != nil {
		return fallBackType
	}

	i, err := strconv.Atoi(strings.TrimSpace(in))
	if err != nil {
		return fallBackType
	}

	return typeForIndex(i)
}
