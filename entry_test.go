package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestNewEntry(t *testing.T) {
	_, err := newEntry(1, "added", "", "")
	if err != ErrNoTitle {
		t.Error("expected a ErrNoTitle")
	}
}

func TestWriteToPath(t *testing.T) {
	e, err := newEntry(1, "added", "new feature mepmep", "")
	if err != nil {
		t.Fatal(err)
	}

	path := filepath.Join(os.TempDir(), "changelog-test.yml")
	defer os.RemoveAll(path)

	if err := e.writeToPath(path, false); err != nil {
		t.Fatal(err)
	}

	// overwriting is not allowed if 'force' is false
	if err := e.writeToPath(path, false); err == nil {
		t.Fatalf("expected an error, got none")
	}

	// overwriting is OK when using force
	if err := e.writeToPath(path, true); err != nil {
		t.Fatalf("overwriting with force didn't work: %s", err)
	}
}

func TestTypeforIndex(t *testing.T) {
	tcs := []struct {
		input  int
		output string
	}{
		{1, "added"},
		{2, "fixed"},
		{8, fallBackType},
		{len(types) + 1, fallBackType},
		{0, fallBackType},
		{100, fallBackType},
	}

	for _, tc := range tcs {
		if typeForIndex(tc.input) != tc.output {
			t.Fatalf("expected %s, got: %s", tc.output, typeForIndex(tc.input))
		}
	}
}
