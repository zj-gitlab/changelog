package main

import (
	"strings"

	"code.gitea.io/git"
)

func lastCommitMessage() (string, error) {
	repo, err := git.OpenRepository(".")
	if err != nil {
		return "", err
	}

	oid, err := repo.GetRefCommitID("HEAD")
	if err != nil {
		return "", err
	}

	commit, err := repo.GetCommit(oid)
	if err != nil {
		return "", err
	}

	return strings.Split(commit.CommitMessage, "\n")[0], nil
}

func onFeatureBranch() bool {
	br, _ := branchName()

	return br != "master"
}

func branchName() (string, error) {
	repo, err := git.OpenRepository(".")
	if err != nil {
		return "", err
	}

	br, err := repo.GetHEADBranch()
	if err != nil {
		return "", err
	}

	return br.Name, nil
}

func stageForCommit(p string) error {
	_, err := git.NewCommand("add", p).Run()
	return err
}

func amendCommit() error {
	_, err := git.NewCommand("commit", "--amend", "--no-edit").Run()
	return err
}

func getConfigKey(key string) (string, error) {
	v, err := git.NewCommand("config", "--get", key).Run()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(v), nil
}

func repoRoot() (string, error) {
	p, err := git.NewCommand("rev-parse", "--show-toplevel").Run()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(p), nil
}
