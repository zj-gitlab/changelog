module gitlab.com/zj/changelog

go 1.12

require (
	code.gitea.io/git v0.0.0-20190411170847-63b74d438b29
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755 // indirect
	github.com/mcuadros/go-version v0.0.0-20190308113854-92cdf37c5b75 // indirect
	github.com/urfave/cli v1.20.0
	gopkg.in/src-d/go-git.v4 v4.11.0 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
