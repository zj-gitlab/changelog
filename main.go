package main

import (
	"fmt"
	"os"
)

var version = "0.0.0"

func main() {
	if err := newApp().Run(os.Args); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
